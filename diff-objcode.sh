# Strip symbol offset of the form: abc <hi+0x123>
strip_symbol_offsets() {
  sed -e 's/[0-9a-f]\+ <\([a-zA-Z0-9_]\+\)+0x[0-9a-f]\+>/\(\&\1 + N\)/g'
}

# Strip the first column object code offset
strip_code_offsets() {
  sed -e 's/^ *[0-9a-f]\+://'
}

# Strip general-purpose registers
strip_gprs() {
    sed -e 's/%[er][abcds][ix]/%reg/g' \
      -e 's/%r1[0-6][db]\?/%reg/g' \
      -e 's/%r[1-9][db]\?/%reg/g' \
      -e 's/%rbp/%reg/g'
}

dump() {
  objdump -d --no-show-raw-insn -l $1 \
    | strip_code_offsets \
    | strip_gprs \
    | strip_symbol_offsets \
    | cat # | sed -e '%[er][bs]p/%reg/g'
}

diff_objcode() {
  f1="$1"
  f2="$2"
  diff -u -w300 <(dump $f1) <(dump $f2) \
    | nix run nixpkgs.diffr -c diffr \
    | less -RC
}

diff_objcode ghc-tsan-base/_build/stage1/rts/build/c/sm/Evac.thr_o ghc-tsan/_build/stage1/rts/build/c/sm/Evac.thr_o
