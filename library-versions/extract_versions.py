#!/usr/bin/env python3

import logging
from pathlib import Path
import subprocess
import tempfile
from typing import Set
import json

def find_packages(bindist_tarball: Path) -> Set[str]:
    logging.info(bindist_tarball)
    with tempfile.TemporaryDirectory() as tmp:
        tmp = Path(tmp)
        subprocess.run(['tar', '-xf', bindist_tarball.resolve()], cwd=tmp, check=True)
        pkgdb = list(tmp.rglob('package.conf.d'))
        confs = [ fn.stem for fn in pkgdb[0].iterdir() if fn.suffix == '.conf' ]
        pkgs = set()
        for conf in confs:
            parts = conf.split('-')
            pkg_name = '-'.join(parts[:-1])
            ver = parts[-1]
            pkgs.add((pkg_name, ver))

        logging.info(bindist_tarball, pkgs)
        return pkgs

def do_version(ver_dir: Path):
    if not ver_dir.is_dir():
        return

    ver = ver_dir.name
    if not ver[0].isnumeric():
        return

    valid_tarball = lambda fn: \
        not fn.name.endswith('.sig') and not fn.name.endswith('.lz')

    linux_tarballs = [ fn for fn in ver_dir.glob('ghc-*-linux*.tar.*') if valid_tarball(fn) ]
    linux_tarball = linux_tarballs[0] if linux_tarballs else None
    windows_tarballs = [ fn for fn in ver_dir.glob('ghc-*-mingw32*.tar.*') if valid_tarball(fn) ]
    windows_tarball = windows_tarballs[0] if windows_tarballs else None
    logging.info(ver, linux_tarball, windows_tarball)

    packages = set()
    if linux_tarball:
        packages |= find_packages(linux_tarball)
    if windows_tarball:
        packages |= find_packages(windows_tarball)

    return packages

def main() -> None:
    root = Path('./ghc/downloads')
    for ver_dir in sorted(root.iterdir(), reverse=True):
        pkgs = do_version(ver_dir)
        if pkgs:
            print(ver_dir.name, json.dumps({ k:v for k,v in pkgs }))

if __name__ == '__main__':
    main()
