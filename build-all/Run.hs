#!/usr/bin/env cabal
{- cabal:
    build-depends:
      base, text, containers,
      typed-process, stm, async,
      directory, temporary, filepath
-}

import Control.Monad
import qualified Data.Text as T
import qualified Data.Map.Strict as M
import Data.List
import Data.Maybe
import System.Process.Typed
import System.Directory
import System.IO
import System.IO.Temp
import System.FilePath
import Control.Exception
import Control.Concurrent.STM
import Control.Concurrent.STM.TSem
import Control.Concurrent.Async

-- | How many cores to use in total
cores :: Int
cores = 40

-- | How many cores to use per build
coresPerBuild :: Int
coresPerBuild = 4

-- | How many cores to use per testsuite run
coresPerTest :: Int
coresPerTest = 12

-- | How many times to run each nofib benchmark
nofibRepeats :: Int
nofibRepeats = 2

-- | Path to the ghc-utils tree (used to locate @ghc_perf.py@).
ghcUtilsPath :: FilePath
ghcUtilsPath = "/home/ben/ghc/ghc-utils"

-- | Which perf events to collect, if any.
perfEvents :: [String]
perfEvents = []

-- | Hackage dependencies necessary to build nofib benchmarks.
nofibDeps :: [String]
nofibDeps = [ "random", "old-time", "parallel"]

data Commit = Commit { commitName :: String
                     , commitRef :: String
                     }
            deriving (Eq, Ord, Show)

sanitisedCommitName :: Commit -> String
sanitisedCommitName = map f . commitName
  where
    f '/' = '-'
    f c   = c

commitWorkDir :: Commit -> FilePath
commitWorkDir c = "ghc-" <> sanitisedCommitName c

commitResultDir :: Commit -> FilePath
commitResultDir c = "results" </> sanitisedCommitName c

withCommitResultFile :: Commit -> FilePath -> (Handle -> IO a) -> IO a
withCommitResultFile c fname action = do
    createDirectoryIfMissing True dir
    withFile (dir </> fname) WriteMode action
  where
    dir = commitResultDir c

commitGhcPath :: Commit -> FilePath
commitGhcPath commit =
  commitWorkDir commit </> "_build" </> "stage1" </> "bin" </> "ghc"

readCommits :: IO [Commit]
readCommits =
    mapMaybe parse . lines <$> readFile "branches.txt"
  where
    parse line =
      case words line of
        [] -> Nothing
        [commit] -> Just $ Commit commit commit
        [name, commit] -> Just $ Commit name commit

data Parallelism = Exclusive
                 | WantsCores Int

data Task = Task { taskName :: String
                 , taskDeps :: [Task]
                 , taskRun  :: Commit -> IO ()
                 , taskParallelism :: Parallelism
                 }

task :: String -> (Commit -> IO ()) -> Task
task name run =
  Task { taskName = name
       , taskDeps = []
       , taskRun  = run
       , taskParallelism = Exclusive
       }

parallel :: Int -> Task -> Task
parallel n task = task { taskParallelism = WantsCores n }

requires :: [Task] -> Task -> Task
requires deps task = task { taskDeps = taskDeps task ++ deps }

logMsg :: String -> IO ()
logMsg = putStrLn

checkout :: Task
checkout = parallel 1 $ task "checkout" $ \commit -> do
  let wdir = commitWorkDir commit
      git args = runProcess_ $ inCommitWorkDir commit $ proc "git" args
  exists <- doesDirectoryExist (commitWorkDir commit)
  unless exists $ do
    logMsg $ concat [ "Checking out "
                    , commitName commit
                    , " ("
                    , commitRef commit
                    , ") at " <> wdir
                    ]
    runProcess_
      $ setWorkingDir "ghc"
      $ proc "git" ["worktree", "add", "-f" , "../"<>wdir, commitRef commit]

  git ["checkout", "--detach", commitRef commit]
  git ["--no-pager", "show"]
  git ["submodule", "update", "--init"]

configure :: Task
configure = requires [checkout] $ task "configure" run
  where
    run commit = do
      let wdir = commitWorkDir commit
      createDirectoryIfMissing True (wdir </> "_build")
      let settingsTemplate = wdir </> "_build" </> "hadrian.settings"
      writeFile settingsTemplate . fromMaybe "" =<< readIfExists "hadrian.settings"

      runWithLogging commit "boot.log" $ inCommitWorkDir commit $ proc "./boot" []
      runWithLogging commit "configure.log" $ inCommitWorkDir commit $ proc "./configure" []

build :: Task
build = parallel coresPerBuild $ requires [configure] $ task "build" run
  where
    run commit =
      runWithLogging commit "build.log"
        $ inCommitWorkDir commit
        $ proc ("hadrian" </> "build-cabal") ["-j" <> show coresPerBuild]

test :: Task
test = parallel coresPerTest $ requires [build] $ task "test" run
  where
    run commit = do
      resultDir <- canonicalizePath $ commitResultDir commit
      runWithLogging commit "test.log"
        $ inCommitWorkDir commit
        $ proc ("hadrian" </> "build-cabal")
        $ [ "-j" <> show coresPerBuild
          , "--summary-metrics=" <> resultDir </> "test-metrics"
          , "test"
          ]

nofibBoot :: Task
nofibBoot = parallel 1 $ requires [build] $ task "boot-nofib" run
  where
    run commit = do
        cwd <- getCurrentDirectory
        runWithLogging commit "build-nofib-deps.log"
          $ inCommitWorkDir commit
          $ proc "cabal"
          $ ["v1-install", "-w", cwd </> commitGhcPath commit, "--allow-newer"]
          ++ nofibDeps

nofibRun :: Task
nofibRun = requires [nofibBoot] $ task "run-nofib" run
  where
    run commit = do
        cwd <- getCurrentDirectory
        runWithLogging commit "run-nofib.log"
          $ setWorkingDir (commitWorkDir commit </> "nofib")
          $ proc "cabal"
          $ [ "new-run", "nofib-run", "--"
            , "--compiler=" <> (cwd </> commitGhcPath commit)
            , "--compiler-arg=-fproc-alignment=64"
            , "--times=" <> show nofibRepeats
            , "--output=" <> (cwd </> commitResultDir commit </> "nofib")
            ] <> concat [ [ "--perf", "--perf-arg=" <> intercalate "," perfEvents] | not (null perfEvents) ]

buildPackage :: String   -- ^ package name
             -> FilePath -- ^ source directory
             -> [String] -- ^ @ghc_perf.py@ arguments
             -> [String] -- ^ compiler arguments
             -> Task
buildPackage name srcPath ghcPerfArgs hcArgs =
    requires [build] $ task name' run
  where
    name' = "build-" <> name
    run commit = do
      withCommitResultFile commit (name'<>".log") $ \hdl -> do
        cwd <- getCurrentDirectory
        resultDir <- canonicalizePath $ commitResultDir commit
        withTempDirectory (commitWorkDir commit) name' $ \tmpDir -> do
          runWithLogging commit (name'<>".log")
            $ setWorkingDir (commitWorkDir commit </> srcPath)
            $ proc (ghcUtilsPath </> "ghc_perf.py")
            $ [ "-n", name'
              , "-o", resultDir </> name' <.> "tsv"
              ] ++ ghcPerfArgs ++
              [ "--"
              , cwd </> commitGhcPath commit
              , "-isrc", "-Iinclude", "-fforce-recomp"
              , "-odir", tmpDir
              , "-hidir", tmpDir
              ] ++ hcArgs

buildCabal :: Task
buildCabal = buildPackage "Cabal" "libraries/Cabal/Cabal" [] ["-package", "mtl", "Setup.hs"]

benchBytestring :: Task
benchBytestring = requires [build] $ task "bench-bytestring" run
  where
    run commit = do
        cwd <- getCurrentDirectory
        resultDir <- canonicalizePath $ commitResultDir commit

        let srcPath = "libraries" </> "bytestring"

        runProcess_
            $ setWorkingDir (commitWorkDir commit </> srcPath)
            $ proc "curl"
            $ [ "-o", "cabal.project.local"
              , "https://ghc.gitlab.haskell.org/head.hackage/cabal.project.local"
              ]

        let dumpDir = resultDir </> "bytestring-dumps"
        createDirectoryIfMissing True dumpDir
        runWithLogging commit "configure-bytestring.log"
          $ setWorkingDir (commitWorkDir commit </> srcPath)
          $ proc "cabal"
          $ [ "configure"
            ] ++ foldMap (\o -> ["--ghc-option="++o])
                   [ "-ddump-to-file", "-dumpdir " ++ dumpDir, "-ddump-simpl", "-ddump-stg", "-dsuppress-all" ]

        runWithLogging commit "bench-bytestring.log"
          $ setWorkingDir (commitWorkDir commit </> srcPath)
          $ proc "cabal"
          $ [ "run"
            , "bench:bytestring-bench"
            , "--"
            , "--csv", cwd </> commitResultDir commit </> "bench-bytestring.csv"
            , "+RTS", "-s"
            ]


benchText :: Task
benchText = requires [build] $ task "bench-text" run
  where
    extractTestData = do
        cwd <- getCurrentDirectory
        let out = cwd </> "text-test-data"
        unlessExists out $ do
            runProcess_
                $ proc "git"
                $ [ "clone", "https://github.com/bos/text-test-data" ]

            runProcess_
                $ setWorkingDir out
                $ proc "bash"
                $ [ "-c", "for f in *.bz2; do bunzip2 $f; done" ]

        return out

    run commit = do
        cwd <- getCurrentDirectory
        resultDir <- canonicalizePath $ commitResultDir commit

        let srcPath = "libraries" </> "text"
            testData = (commitWorkDir commit </> srcPath </> "benchmarks" </> "text-test-data")
        unlessExists testData $ do
            testDataSrc <- extractTestData
            createFileLink (cwd </> testDataSrc) testData

        runProcess_
            $ setWorkingDir (commitWorkDir commit </> srcPath)
            $ proc "curl"
            $ [ "-o", "cabal.project.local"
              , "https://ghc.gitlab.haskell.org/head.hackage/cabal.project.local"
              ]

        runWithLogging commit "bench-text.log"
          $ setWorkingDir (commitWorkDir commit </> srcPath)
          $ proc "cabal"
          $ [ "run"
            , "bench:text-benchmarks"
            , "--"
            , "--csv", cwd </> commitResultDir commit </> "bench-text.csv"
            , "+RTS", "-s"
            ]

runWithLogging
    :: Commit -> FilePath
    -> ProcessConfig stdin stdout stderr
    -> IO ()
runWithLogging commit logFile config =
    withCommitResultFile commit logFile $ \hdl ->
        runProcess_
          $ setStderr (useHandleOpen hdl)
          $ setStdout (useHandleOpen hdl)
          $ config

inCommitWorkDir
    :: Commit
    -> ProcessConfig stdin stdout stderr
    -> ProcessConfig stdin stdout stderr
inCommitWorkDir commit = setWorkingDir (commitWorkDir commit)

unlessExists :: FilePath -> IO () -> IO ()
unlessExists path action = do
    exists <- doesPathExist path
    unless exists action

readIfExists :: FilePath -> IO (Maybe String)
readIfExists fname = do
  exists <- doesFileExist fname
  case exists of
    True -> Just <$> readFile fname
    False -> return Nothing

-- | A semaphore that also supports exclusive access.
data ExclSem = ExclSem Int TSem

newExclSem :: Int -> IO ExclSem
newExclSem n =
  atomically $ fmap (ExclSem n) $ newTSem (fromIntegral n)

withExclusive :: ExclSem -> IO a -> IO a
withExclusive (ExclSem n sem) = bracket_ take release
  where
    take = atomically $ replicateM n $ waitTSem sem
    release = atomically $ signalTSemN (fromIntegral n) sem

withNonexclusive :: ExclSem -> Int -> IO a -> IO a
withNonexclusive (ExclSem n sem) m action
  | n < m = fail $ "withNonexclusive: n="<>show n<>", m="<>show m
  | otherwise = bracket_ take release action
  where
    take = atomically $ replicateM m $ waitTSem sem
    release = atomically $ signalTSemN (fromIntegral m) sem

allTasks :: [Task]
allTasks =
    [ checkout
    , build
    , test
    , nofibBoot
    , nofibRun
    , buildCabal
    , benchText
    , benchBytestring
    ]

runTasks :: Int -> [Commit] -> [Task] -> IO ()
runTasks cores commits tasks = do
  sem <- newExclSem cores
  let allTasks = foldMap closeTask tasks
      closeTask :: Task -> [Task]
      closeTask t = foldMap closeTask (taskDeps t) ++ [t]
  putStrLn $ "running tasks " <> show (map taskName allTasks) <> " for commits " <> show commits
  asyncs <- M.fromList <$> sequence
    [ do a <- async $ mapM_ (runTask sem commit) allTasks
         return (commit, a)
    | commit <- commits
    ]
  excs <- mapM waitCatch asyncs
  forM_ (M.toList excs) $ \(commit, result) ->
    putStrLn $ commitName commit ++ ": " ++ show result

runTask :: ExclSem -> Commit -> Task -> IO ()
runTask sem commit task = withSem $ do
    done <- checkStamp task commit
    unless done $ do
      putStrLn $ "Running task " <> taskName task <> " for " <> show commit
      mapM_ assertStamp (taskDeps task)
      taskRun task commit
      mkStamp task commit
  where
    assertStamp task' = do
      exists <- checkStamp task' commit
      unless exists $ fail
        $ commitName commit <> ": task `"
          <> taskName task <> "` needs prerequisitie task `" <> taskName task' <> "`."

    withSem = case taskParallelism task of
                Exclusive -> withExclusive sem
                WantsCores n -> withNonexclusive sem n


stampPath :: Task -> Commit -> FilePath
stampPath task commit =
  commitWorkDir commit </> ".stamp-" <> taskName task

mkStamp :: Task -> Commit -> IO ()
mkStamp task commit = writeFile (stampPath task commit) ""

checkStamp :: Task -> Commit -> IO Bool
checkStamp task commit = doesFileExist (stampPath task commit)

main :: IO ()
main = do
  exists <- doesDirectoryExist "ghc"
  unless exists $ fail "ghc/ should be a GHC checkout"
  commits <- readCommits
  print commits
  let tasks = [checkout, build, test, benchBytestring, benchText]
  --let tasks = allTasks
  runTasks cores commits tasks

