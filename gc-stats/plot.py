#!/usr/bin/env python3

import matplotlib.pyplot as pl
import numpy as np
import json
import sys

fname = sys.argv[1]
a = json.load(open(fname))
#a = np.genfromtxt(fname, skip_footer=1)
gen = [c['generation'] for c in a]

print(a[0])
#gens = np.unique(gen)
for g in [0,1]:
    #pl.subplot(len(gens), 1, g+1)
    colls = [ coll for coll in a
              if coll['generation'] == g
            ]

    total_times = [ max((interval[-1][1] - coll['requestTime']) / 1e3
                        for interval in coll['gcWorkIntervals'].values()
                        )
                    for coll in colls ]
    sync_times = [ max(t - coll['requestTime']
                       for t in coll['startTimes'].values())
                   for coll in colls ]

    print(sync_times)
    #pl.gca().set_title('Generation %d' % g)
    pl.subplot(211)
    pl.hist(total_times,
            label='generation %d' % g,
            bins=np.logspace(-6, 0, 100))
    pl.axvline(np.mean(total_times))
    pl.xscale('log')

    pl.subplot(212)
    pl.hist(sync_times)
    pl.xscale('log')

pl.savefig('out.svg')
