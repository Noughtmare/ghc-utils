-- | Extract intervals book-ended by @traceMarker@
-- events from an eventlog.
module Samples where

import GHC.RTS.Events
import Data.Maybe
import Types
import Data.List
import qualified Data.Text as T

isUserMarker :: EventInfo -> Maybe T.Text
isUserMarker (UserMarker msg) = Just msg
isUserMarker _ = Nothing

isStart :: String -> EventInfo -> Bool
isEnd   :: String -> EventInfo -> Bool
isStart lbl = fromMaybe False . fmap (T.pack ("start:"<>lbl<>":") `T.isPrefixOf`) . isUserMarker
isEnd   lbl = fromMaybe False . fmap (T.pack ("end:"  <>lbl<>":") `T.isPrefixOf`) . isUserMarker

toIntervals :: String -> [Event] -> [Interval]
toIntervals lbl = goStart
  where
    goStart (ev:rest)
      | isStart lbl (evSpec ev) = goEnd (evTime ev) rest
    goStart (_:rest)            = goStart rest
    goStart []                  = []

    goEnd t (ev:rest)
      | isEnd lbl (evSpec ev)   = (Interval t (evTime ev)) : goStart rest
    goEnd t (_:rest)            = goEnd t rest
    goEnd t []                  = []
