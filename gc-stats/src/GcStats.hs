{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}

module GcStats where

import GHC.Generics
import Data.Aeson (ToJSON(..), ToJSONKey)
import Data.Monoid
import qualified Data.Map as M
import qualified Data.Set as S
import Data.Maybe
import Data.Word
import GHC.RTS.Events
import Types

data Gc = Gc { requestor     :: Cap
             , parallel      :: Bool
             , generation    :: Int
             , requestTime   :: Timestamp
             , startTimes    :: M.Map Cap Timestamp -- ^ when each capability starts GC
             , gcWorkIntervals :: M.Map Cap (S.Set Interval) -- ^ when each capability was working
             , bytesCopied   :: Word64
             , gcEndTimes    :: M.Map Cap Timestamp
             }
        deriving (Show, Generic)
instance ToJSON Gc

readGcs :: FilePath -> IO [Gc]
readGcs inFile = do
    Right elog <- readEventLogFromFile inFile
    return $ toGcs $ sortEvents $ events $ dat elog

toGcs :: [Event] -> [Gc]
toGcs = go Nothing mempty
  where
    go :: Maybe Gc -> M.Map Cap Timestamp -> [Event] -> [Gc]
    go gc _workStart [] = maybeToList gc
    go gc _workStart  (Event t ev (Just cap) : rest)
      --- | not $ M.null workStart = error $ "non-empty workStart: "++show workStart
      | RequestParGC <- ev =
          maybeToList gc ++ go (Just $ gc0 { parallel = True})  mempty rest
      | RequestSeqGC <- ev =
          maybeToList gc ++ go (Just $ gc0 { parallel = False}) mempty rest
      where gc0 = Gc { requestor     = Cap cap
                     , parallel      = undefined
                     , generation    = undefined
                     , requestTime   = t
                     , startTimes    = mempty
                     , gcWorkIntervals = mempty
                     , bytesCopied   = undefined
                     , gcEndTimes    = mempty
                     }

    go (Just gc) workStart (Event t ev (Just cap') : rest)
      | GCIdle <- ev
      , Just start <- M.lookup cap workStart =
          let workStart' = M.delete cap workStart
              int = Interval start t
          in go (Just $ gc { gcWorkIntervals = M.insertWith (<>) cap (S.singleton int) (gcWorkIntervals gc) }) workStart' rest
      | GCWork <- ev
      , cap `M.member` workStart = error "worker never started"
      | GCWork <- ev =
          go (Just gc) (M.insert cap t workStart) rest
      where cap = Cap cap'

    go (Just gc) workStart (Event t ev (Just cap') : rest)
      | StartGC <- ev =
          go (Just $ gc { startTimes = M.insert cap t (startTimes gc) }) workStart rest
      | GCStatsGHC {..} <- ev =
          go (Just $ gc { generation = gen, bytesCopied = parTotCopied }) workStart rest
      | EndGC <- ev
      , Just start <- M.lookup cap workStart =
          let workStart' = M.delete cap workStart
              int = Interval start t
          in go (Just $ gc { gcEndTimes = M.insert cap t (gcEndTimes gc)
                           , gcWorkIntervals = M.insertWith (<>) cap (S.singleton int) (gcWorkIntervals gc)
                           }) workStart' rest
      | EndGC <- ev =
          let workStart' = M.delete cap workStart
          in go (Just $ gc { gcEndTimes = M.insert cap t (gcEndTimes gc) }) workStart' rest
      where cap = Cap cap'
    --go gc (Event t ev Nothing : rest)
    go gc workStart (_:rest) = go gc workStart rest

maxStartLatency :: Gc -> Timestamp
maxStartLatency gc =
    maximum [ t - requestTime gc
            | t <- M.elems $ startTimes gc
            ]

gcEndTime :: Gc -> Timestamp
gcEndTime = maximum . gcEndTimes

endIdleTimes :: Gc -> [Timestamp]
endIdleTimes gc =
    [ endTime - maximum (map intEnd $ S.toList workIntervals)
    | workIntervals <- M.elems $ gcWorkIntervals gc
    ]
  where endTime = gcEndTime gc
