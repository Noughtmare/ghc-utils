{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}

module TickyReport.Eventlog
    ( readEventlog ) where

import Data.Word
import qualified Data.Map.Strict as M
import qualified Data.Text as T
import GHC.RTS.Events

import TickyReport.Types

readEventlog :: FilePath -> IO TickyReport
readEventlog fp = do
    Right elog <- readEventLogFromFile fp
    return $ parseEvents elog

newtype CounterId = CounterId Word64
    deriving (Eq, Ord, Show)

parseEvents :: EventLog -> TickyReport
parseEvents elog =
    TickyReport frames
  where
    frames :: [TickyFrame]
    frames =
      [ TickyFrame stats arguments stgName
      | ev <- events (dat elog)
      , TickyCounterDef cid _arity kinds name <- pure $ evSpec ev
      , let stats = M.findWithDefault mempty (CounterId cid) counts
      , let arguments = "" -- TODO
      , let defMod = ModName "" "" -- TODO
      , let stgName = StgName
              { stgnExported = False -- TODO
              , _stgnDefiningModule = defMod
              , _stgnName = T.unpack name
              , stgnSignature = T.unpack kinds
              , stgnParent = Nothing -- TODO
              }
      ]

    counts :: M.Map CounterId TickyStats
    counts = M.fromListWith (<>)
      [ (CounterId cid, stats)
      | ev <- events (dat elog)
      , TickyCounterSample cid entries alloc allocd <- pure $ evSpec ev
      , let stats = TickyStats
              { entries = fromIntegral entries
              , alloc = fromIntegral alloc
              , allocd = fromIntegral allocd
              }
      ]

