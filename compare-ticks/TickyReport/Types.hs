module TickyReport.Types
    ( TickyReport(..)
    , TickyFrame(..)
    , TickyStats(..)
    , ClosureKind(..)
    , ModuleName(..)
    , pprModuleName
    , StgName(..)
    , pprStgName
    , stgnName
    , stgnDefiningModule
    ) where

import Data.Monoid
import Prelude

data TickyReport = TickyReport { frames :: [TickyFrame] }
                 deriving (Show)

data TickyFrame = TickyFrame { stats     :: TickyStats
                             , arguments :: String
                             , stgName   :: StgName
                             }
                deriving (Show)

data TickyStats = TickyStats { entries   :: Integer
                             , alloc     :: Integer
                             , allocd    :: Integer
                             }
                deriving (Show, Eq)

instance Semigroup TickyStats where
    TickyStats a b c <> TickyStats x y z = TickyStats (a+x) (b+y) (c+z)

instance Monoid TickyStats where
    mempty = TickyStats 0 0 0

data ClosureKind = Func { singleEntry :: Bool }
                 | Thunk { stdThunk :: Bool, singleEntry :: Bool }
                 | Con String
                 deriving (Show, Eq)

-- | The name of a module
data ModuleName = ModName { modulePackage :: String
                          , moduleName    :: String
                          }
                deriving (Show)

pprModuleName :: ModuleName -> String
pprModuleName (ModName pkg name) = pkg<>":"<>name

-- | An STG name
--
-- An exported name like @ghc-7.11:CmdLineParser.runCmdLine{v rWL}@ is,
--
-- @
-- StgName { definingModule = Just $ ModName (Just "ghc-7.11") "CmdLineParser"
--         , name = "runCmdLine"
--         , signature = "v rWL"
--         , parent = Nothing
--         }
-- @
--
-- A non-exported name like @sat_s5EP{v} (ghc-7.11:CmdLineParser) in rXu@ is,
--
-- @
-- StgName { definingModule = Nothing
--         , name = "sat_s5EP"
--         , signature = "v"
--         , parent = "rXu"
--         }
-- @
data StgName = StgName { stgnExported       :: Bool
                       , _stgnDefiningModule :: ModuleName
                       , _stgnName          :: String
                       , stgnSignature      :: String
                       , stgnParent         :: Maybe String
                       }
             | Top
             deriving (Show)

pprStgName :: StgName -> String
pprStgName Top = "Top"
pprStgName (StgName _exported modname name _sig _parent) =
    name<>" ("<>pprModuleName modname<>")"

stgnName :: StgName -> String
stgnName (StgName {_stgnName=n}) = n
stgnName Top = "Top"

stgnDefiningModule :: StgName -> ModuleName
stgnDefiningModule Top = ModName "" ""
stgnDefiningModule (StgName _ modname _ _ _) = modname

