#!/usr/bin/env python3

from matplotlib import pyplot as pl

opened_tickets = {
    2000 :             15,
    2001 :            131,
    2002 :             73,
    2003 :             96,
    2004 :             91,
    2005 :            236,
    2006 :            433,
    2007 :            933,
    2008 :            893,
    2009 :            893,
    2010 :            817,
    2011 :            855,
    2012 :            967,
    2013 :           1105,
    2014 :           1261,
    2015 :           1370,
    2016 :           1514,
    2017 :           1573,
    2018 :           1494,
    2019 :           1496,
    2020 :           1509,
    2021 :           1749,
    2022 :           1235,
}

closed_tickets = {
    2001 :             70,
    2002 :             73,
    2003 :             95,
    2004 :             65,
    2005 :            171,
    2006 :            272,
    2007 :            693,
    2008 :            719,
    2009 :            781,
    2010 :            765,
    2011 :            842,
    2012 :            796,
    2013 :            901,
    2014 :           1020,
    2015 :           1156,
    2016 :           1069,
    2017 :           1058,
    2018 :           1053,
    2019 :            959,
    2020 :            951,
    2021 :           1288,
    2022 :            948,
}

pl.rcParams.update({'font.size': 22})
pl.figure(figsize=(16,9))
pl.plot(opened_tickets.keys(), opened_tickets.values(), 'o-', label='opened tickets')
pl.plot(closed_tickets.keys(), closed_tickets.values(), 'o-', label='closed tickets')
pl.suptitle('Tickets over time\n(by calendar year)')
pl.xlabel('year')
pl.ylabel('number of tickets')
pl.legend()
pl.savefig('tickets.svg')
