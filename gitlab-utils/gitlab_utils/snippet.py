#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gitlab
import click

@click.command()
@click.argument('file', type=click.File('r'))
@click.option('--title', '-t', type=str)
@click.option('--filename', '-n', type=str)
def cli(file: click.File, title: str, filename: str):
    gl = gitlab.Gitlab.from_config('haskell')
    proj = gl.projects.get(1)
    snippet = gl.snippets.create({
        'title': title,
        'file_name': filename,
        'content': file.read()
    })
    snippet.save()
    print(snippet)

if __name__ == '__main__':
    cli()

