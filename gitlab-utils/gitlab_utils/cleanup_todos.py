#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import gitlab

def main():
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    
    gl = gitlab.Gitlab.from_config()
    for todo in gl.todos.list(all=True, lazy=True):
        if todo.author['username'] == 'marge-bot':
            todo.mark_as_done()
        elif todo.target_type == 'MergeRequest' \
                and todo.action_name in ['approval_required', 'build_failed'] \
                and todo.target['state'] in ['merged',  'closed']:
            todo.mark_as_done()
        elif todo.action_name == 'build_failed':
            todo.mark_as_done()

main()
