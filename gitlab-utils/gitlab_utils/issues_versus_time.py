#!/usr/bin/env python3

import gitlab
from matplotlib import pyplot as pl
from datetime import *

def parse_date(s):
    if s is None:
        return None
    return datetime.strptime(s[0:s.find('T')], '%Y-%m-%d').date()

def get_issues():
    gl = gitlab.Gitlab.from_config()
    proj = gl.projects.get(1)

    issues = [(parse_date(i.created_at), parse_date(i.closed_at)) for i in proj.issues.list(all=True, lazy=True)]
    return issues

start_date = date(2001, 1, 1)
end_date = date.today()

def date_range(start, end):
    d = start
    while d < end:
        yield d
        d = d + timedelta(days=1)

def plot_open(issues) -> None:
    open_tickets = { day : len([i for i in issues if i[0] < day and (i[1] is None or i[1] > day)]) for day in date_range(start_date, end_date) }
    all_tickets = { day : len([i for i in issues if i[0] < day ]) for day in date_range(start_date, end_date) }
    fraction = { day: open_tickets[day] / all_tickets[day] for day in open_tickets.keys() }

    pl.rcParams.update({'font.size': 22})
    pl.figure(figsize=(16,9))
    pl.plot(open_tickets.keys(), open_tickets.values(), label='open tickets')
    pl.plot(all_tickets.keys(), all_tickets.values(), label='all tickets')
    pl.legend()
    pl.xlabel('date')
    pl.ylabel('number of open tickets')
    pl.suptitle('Number of open tickets versus time')
    pl.savefig('open-tickets.svg')

    pl.figure(figsize=(16,9))
    pl.plot(fraction.keys(), fraction.values())
    pl.xlabel('date')
    pl.ylabel('fraction of tickets open')
    pl.suptitle('Fraction of open tickets versus time')
    pl.savefig('open-fraction.svg')

def main() -> None:
    issues = get_issues()
    plot_open(issues)
