#!/usr/bin/env python3

from typing import List, Optional, Set
from pathlib import Path
from tempfile import TemporaryDirectory
import subprocess

cap_fields = {
        'f',
        'r',
        'r.rSpLim',
        'no',
        'running_task',
        'in_haskell',
        'idle', 
        'disabled',
        'run_queue_hd',
        'run_queue_tl',
        'n_run_queue',
        'suspended_ccalls',
        'n_suspended_ccalls',
        'mut_lists',
        'saved_mut_lists',
        'upd_rem_set',
        'current_segments',
        'pinned_object_block',
        'pinned_object_blocks',
        'pinned_object_empty',
        'context_switch',
        'interrupt',
        'total_allocated',
}

def find_field_offset(headers: List[str], ty: str, field: str, cc: str='cc', cflags: List[str]=[]) -> Optional[int]:
    with TemporaryDirectory() as d:
        dd = Path(d)
        s = '\n'.join(
            [ f'#include "{header}"' for header in headers] +
            [ '#include <stdio.h>',
              '#include <stdint.h>',
              'int main() {',
              f'  printf("%d\\n", (uintptr_t) &(({ty}*) 0)->{field});'
              '   return 0;',
              '}',
            ])
        f = dd / 'test.c'
        f.write_text(s)
        exe = dd / 'test'
        subprocess.run([cc, f, '-o', exe] + cflags, check=True)
        p = subprocess.run([exe], check=True, capture_output=True)
        return int(p.stdout)

def offsets_table(headers: List[str], ty: str, fields: Set[str], cc: str='cc', cflags: List[str]=[]) -> str:
    offsets = sorted([
        (find_field_offset(
            headers = headers,
            ty = ty,
            field = field,
            cc = cc,
            cflags = cflags
        ), field)
        for field in fields
    ])

    s = ''
    for offset, field in offsets:
        s += f'{hex(offset)}\t{field}\n'

    return s

print(offsets_table(
    headers = ['rts/PosixSource.h', 'Rts.h', 'Capability.h'],
    ty = 'Capability',
    fields = cap_fields,
    cc='ghc',
    cflags=['-no-hs-main', '-Irts', '-Irts/include']
))

