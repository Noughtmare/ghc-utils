#!/usr/bin/env bash

# N.B. I captured to video using OBS
nix run nixpkgs#gource -- --output-custom-log ghc.txt .
#grep -v -e /ghc/testsuite/ -e /tests/ -e /gmp/ ghc.txt > filtered.txt
grep -e /compiler/ -e /rts/ ghc.txt > filtered.txt
nix run nixpkgs#gource -- --disable-bloom --seconds-per-day 0.1 --hide filenames --title GHC filtered.txt --key

