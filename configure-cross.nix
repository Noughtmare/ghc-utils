let
  mkCrossConfig = { target ? "riscv64" }:
    let
      pkgs = import <nixpkgs> {};
      crossPkgs = pkgs.pkgsCross.${target};
      prefix = crossPkgs.stdenv.cc.targetPrefix;
      triple = crossPkgs.stdenv.targetPlatform.system;
    in
      pkgs.writeTextFile {
        name = "env";
        text = ''
          export GHC=${pkgs.ghc}/bin/ghc
          export CC=${crossPkgs.stdenv.cc}/bin/${prefix}cc
          export CXX=${crossPkgs.stdenv.cc}/bin/${prefix}c++
          export AR=${crossPkgs.stdenv.cc.bintools.bintools}/bin/${prefix}ar
          export RANLIB=${crossPkgs.stdenv.cc.bintools.bintools}/bin/${prefix}ranlib
          export NM=${crossPkgs.stdenv.cc.bintools.bintools}/bin/${prefix}nm
          export LD=${crossPkgs.stdenv.cc.bintools}/bin/${prefix}ld
          export PATH=${pkgs.m4}/bin:${pkgs.autoconf}/bin:${pkgs.automake}/bin:$PATH
          export CONFIGURE_ARGS="--enable-unregisterised --target=${triple} --with-intree-gmp"

          export HAPPY=${pkgs.haskellPackages.happy}/bin/happy
          export ALEX=${pkgs.haskellPackages.alex}/bin/alex

          configure_ghc() {
            ./configure $CONFIGURE_ARGS
          }
        '';
      };
in {
  riscv64 = mkCrossConfig { target = "riscv64"; };
  aarch64 = mkCrossConfig { target = "aarch64-multiplatform"; };
  i386 = mkCrossConfig { target = "gnu32"; };
  # N.B. This is broken due to https://github.com/NixOS/nixpkgs/issues/144126.
  mingw64 = mkCrossConfig { target = "mingwW64"; };
  mk = mkCrossConfig;
}

